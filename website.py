from flask import Flask
from flask import render_template
import requests, re
from bs4 import BeautifulSoup

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True

@app.route('/') 

def hello_worldd():
  #return 'Hello, Michaeld!'
  return render_template("hello.html")

@app.route('/result') #scraper
def get_result():
  URL1 = 'https://www.worldometers.info/coronavirus'
  page1 = requests.get(URL1)
  soup1 = BeautifulSoup(page1.content, 'html.parser')
  allnumbers = soup1.find_all('td')
  todayUS = allnumbers[154]
  print(todayUS)
  return render_template("result.html", todayUS = todayUS)

@app.route('/game')
def paddle():
  return render_template("game.html")

app.run(host='0.0.0.0', port= 8081, debug=True)
