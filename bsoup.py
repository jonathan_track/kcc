from flask import Flask
from flask import render_template
from bs4 import BeautifulSoup
import requests, re

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True

@app.route('/') 

def hello_worldd():
  #return 'Hello, Michaeld!'
  return render_template("bsoup.html")

@app.route('/result') #scraper
def get_result():
	#source = requests.get("https://www.worldometers.info/coronavirus/").text

	html_doc = """
	<html><head><title>The Dormouse's story</title></head>
	<body>
	<p class="title"><b>The Dormouse's story</b></p>

	<p class="story">Once upon a time there were three little sisters; and their names were
	<a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
	<a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
	<a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
	and they lived at the bottom of a well.</p>

	<p class="story">...</p>
	"""
	soup = BeautifulSoup(html_doc, 'html.parser')

	print(soup)
	return soup.prettify()

	#return source

@app.route('/game')
def paddle():
  return render_template("game.html")

app.run(host='0.0.0.0', port= 8008, debug=True)
